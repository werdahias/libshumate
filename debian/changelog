libshumate (1.2~beta-1) unstable; urgency=medium

  * New upstream release
  * Build-depend on json-glib, protobuf-c and gperf for new upstream
  * Added new symbols in libshumate1.0-1.symbols
  * Updated version dependencies for new upstream

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 12 Feb 2024 23:02:30 +0100

libshumate (1.2~alpha-1) unstable; urgency=medium

  * New upstream release
  * Updated libshumate-1.0-1.symbols for new symbols

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 17 Jan 2024 22:53:25 +0100

libshumate (1.1.2-3) unstable; urgency=medium

  * Team upload

  [ Matthias Geiger ]
  * Explicitly build-depend on libgirepository1.0-dev and
    gir1.2-freedesktop-dev. (Closes: #1060994)
    These were previously pulled in as a result of a packaging issue in
    graphene (<< 1.10.8-3) which has now been fixed.
    libgirepository1.0-dev provides GLib-2.0.gir in a location compatible
    with vapigen (see #1060904).
    gir1.2-freedesktop-dev should ideally be depended on by libgtk-4-dev,
    but that is not the case yet.
  * Updated my mail address in d/control and d/copyright

  [ Simon McVittie ]
  * Cherry-pick selected packaging commits from debian/latest branch
    (see above)
  * Amend changelog above to mention why libgirepository1.0-dev and
    gir1.2-freedesktop-dev now need to be explicitly build-depended on
  * d/control: Build-depend on GIR XML that is explicitly included.
    These modules are listed in libshumate_gir_includes in the build
    system. (Helps: #1030223)
  * Add ${gir:Depends}, ${gir:Provides} to -dev package.
    See recent gobject-introspection mini-policy versions for details.
    (Helps: #1030223)

 -- Simon McVittie <smcv@debian.org>  Wed, 17 Jan 2024 02:13:50 +0000

libshumate (1.1.2-2) unstable; urgency=medium

  * Build vala bindings (Closes: #1055333)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 06 Nov 2023 16:51:14 +0200

libshumate (1.1.2-1) unstable; urgency=medium

  * New upstream release
  * Stop using debian/control.in and dh_gnome_clean

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 06 Nov 2023 14:50:48 +0200

libshumate (1.1.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Nathan Pratta Teodosio <nathan.teodosio@canonical.com>  Tue, 24 Oct 2023 11:31:07 +0200

libshumate (1.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 18 Sep 2023 09:39:15 -0400

libshumate (1.1~rc-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 05 Sep 2023 15:51:40 -0400

libshumate (1.1~beta-1) unstable; urgency=medium

  * New upstream release
  * Build-Depend on libsysprof-capture-4-dev
  * debian/rules: Set -Dauto_features=enabled
  * debian/libshumate-1.0-1.symbols: Add new symbols
  * Update standards version to 4.6.2, no changes needed

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 01 Sep 2023 08:37:55 -0400

libshumate (1.0.5-1) unstable; urgency=medium

  * New upstream release

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Thu, 20 Jul 2023 15:14:32 +0200

libshumate (1.0.3-2) unstable; urgency=medium

  * Set GTK_A11Y=none for dh_auto_test (Closes: #1028891)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sat, 14 Jan 2023 09:16:26 -0500

libshumate (1.0.3-1) unstable; urgency=medium

  * New upstream release
  * Source only upload for migration to testing

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Thu, 08 Dec 2022 18:07:45 +0100

libshumate (1.0.2-1) unstable; urgency=medium

  * New upstream release (LP: #1996807)

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 05 Nov 2022 17:48:24 +0100

libshumate (1.0.1-2) unstable; urgency=medium

  * Move shumate-1.0.pc to -dev package (Closes: #1020885)

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Mon, 03 Oct 2022 14:34:45 -0700

libshumate (1.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 19 Sep 2022 08:12:20 -0400

libshumate (1.0.0~beta-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 31 Aug 2022 16:04:49 -0400

libshumate (1.0.0~alpha.1+20220818-2) unstable; urgency=medium

  * Rebuild after NEW acceptance

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 28 Aug 2022 16:26:45 -0400

libshumate (1.0.0~alpha.1+20220818-1) unstable; urgency=medium

  * New upstream git snapshot needed for GNOME Maps 43
  * Add libshumate-common package to install translations
  * Bump minimum glib to 2.68.0
  * debian/libshumate-1.0-1.symbols: Update

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 25 Aug 2022 14:01:56 -0400

libshumate (1.0.0~alpha.1-3) unstable; urgency=medium

  * Build with libsoup3

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 23 Aug 2022 13:10:10 -0400

libshumate (1.0.0~alpha.1-2) unstable; urgency=medium

  * Rebuild after NEW acceptance

 -- Jeremy Bicha <jbicha@ubuntu.com>  Fri, 01 Jul 2022 16:28:35 -0400

libshumate (1.0.0~alpha.1-1) unstable; urgency=medium

  * Initial release (Closes: #1006533)

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Wed, 15 Jun 2022 15:58:01 -0400
