# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libshumate/issues\n"
"POT-Creation-Date: 2022-09-03 11:55+0000\n"
"PO-Revision-Date: 2022-09-05 22:45+0300\n"
"Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>\n"
"Language-Team: Latvian <lata-l10n@googlegroups.com>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 :"
" 2);\n"
"X-Generator: Lokalize 21.12.3\n"

#. m is the unit for meters
#: shumate/shumate-scale.c:197
#, c-format
msgid "%d m"
msgstr "%d m"

#. km is the unit for kilometers
#: shumate/shumate-scale.c:200
#, c-format
msgid "%d km"
msgstr "%d km"

#. ft is the unit for feet
#: shumate/shumate-scale.c:206
#, c-format
msgid "%d ft"
msgstr "%d pēdas"

#. mi is the unit for miles
#: shumate/shumate-scale.c:209
#, c-format
msgid "%d mi"
msgstr "%d jūdzes"
