# Finnish translation for libshumate.
# Copyright (C) 2022 libshumate's COPYRIGHT HOLDER
# This file is distributed under the same license as the libshumate package.
# Jiri Grönroos <jiri.gronroos@iki.fi>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: libshumate main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libshumate/issues\n"
"POT-Creation-Date: 2023-05-05 13:38+0000\n"
"PO-Revision-Date: 2023-08-07 18:55+0300\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>\n"
"Language-Team: Finnish <lokalisointi-lista@googlegroups.com>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: data/shumate-simple-map.ui:39
msgid "Zoom In"
msgstr "Lähennä"

#: data/shumate-simple-map.ui:53
msgid "Zoom Out"
msgstr "Loitonna"

#. m is the unit for meters
#: shumate/shumate-scale.c:197
#, c-format
msgid "%d m"
msgstr "%d m"

#. km is the unit for kilometers
#: shumate/shumate-scale.c:200
#, c-format
msgid "%d km"
msgstr "%d km"

#. ft is the unit for feet
#: shumate/shumate-scale.c:206
#, c-format
msgid "%d ft"
msgstr "%d ft"

#. mi is the unit for miles
#: shumate/shumate-scale.c:209
#, c-format
msgid "%d mi"
msgstr "%d mi"
